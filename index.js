var jsdom = require("jsdom");
const { JSDOM } = jsdom;
const { window } = new JSDOM();
const { document } = (new JSDOM('')).window;
global.document = document;

var $ = jQuery = jquery = require('jquery')(window);
const puppeteer = require('puppeteer');
const tabletojson = require('tabletojson');
const fs = require('fs');

const chromeExec = "/opt/google/chrome/chrome";
const nglAdress = "https://www.ngl.one/proclub/league/ps4/1.%20Pro%20Club%20Liga/101347";
const eslAdress = "https://play.eslgaming.com/fifa/playstation/fifa18-ps4/open/1-german-proclub-league-season3/rankings/";
const pclAdress = "https://virtualprogaming.com/league/58";
const headless = true;

const scrapeNGL = function (baseAdress) {
    console.log("Scraping NGL");
    (async () => {
        const browser = await puppeteer.launch({ args: ['--no-sandbox'], executablePath: chromeExec, headless: headless });
        const page = await browser.newPage();
        await page.goto(baseAdress);
        const data = await page.$eval('#ngl-pro > section > div > div.ngl-tab-container > div:nth-child(2) > div > div > table', table => table.innerHTML);
        let nglTable = new Array;
        let converted = tabletojson.convert("<table>" + data + "</table>");
        converted = converted[0].slice(0);
        for (let i = 0; i <= 15; i++) {
            nglTable[i] = new Object;
            nglTable[i]['#'] = converted[i]['#'] + ".";
            nglTable[i]['Name'] = converted[i]['Punkte'];
            nglTable[i]['Spiele'] = converted[i]['S']
                + " - " + converted[i]['U']
                + "-" + converted[i]['N']
                + "-" + converted[i]['T'];
            nglTable[i]['Tordifferenz'] = converted[i]['GT'] + ":" + converted[i]['TD'];
            nglTable[i]['Punkte'] = converted[i]['Spiele'];
        }
        fs.writeFileSync("./data/ngl.json", JSON.stringify(nglTable));
        browser.close();

    })();
};

const scrapeESL = function (baseAdress) {
    console.log("Scraping ESL");
    (async () => {
        const browser = await puppeteer.launch({ args: ['--no-sandbox'], executablePath: chromeExec, headless: headless });
        const page = await browser.newPage();
        await page.goto(baseAdress);
        const data = await page.$eval('body > div.l-page > div.l-main > div > div.l-content > article > div > div > div > div > div > table:nth-child(2) > tbody', table => table.innerHTML);
        let converted = tabletojson.convert("<table>" + data + "</table>");
        converted = converted[0].slice(3);
        let eslTable = new Array;
        let j = 1;
        for (let i = 0; i <= 9; i++) {
            eslTable[i] = new Object;
            eslTable[i]['#'] = converted[j]['0'];
            eslTable[i]['Name'] = converted[j]['1'];
            eslTable[i]['Spiele'] = converted[j]['6'] + " - " + converted[j]['7'];
            eslTable[i]['Tordifferenz'] = converted[j]['8'];
            eslTable[i]['Punkte'] = converted[j]['5'];
            j += 2;

        }
        fs.writeFileSync("./data/esl.json", JSON.stringify(eslTable));

        browser.close();

    })();
};

const scrapePCL = function (baseAdress) {
    console.log("Scraping PCL");
    (async () => {
        const browser = await puppeteer.launch({ args: ['--no-sandbox'], executablePath: chromeExec, headless: headless });
        const page = await browser.newPage();
        await page.goto(baseAdress);
        const data = await page.$eval('#content > div > div > div.postcontent.notopmargin.nobottommargin.col_last.clearfix > div:nth-child(1) > table > tbody', table => table.innerHTML);
        let converted = tabletojson.convert("<table>" + data + "</table>");
        let j = 0;
        let buffer = new Array;
        for (let i = 0; i < 36; i+=2) {
            buffer[j] = converted[0][i];
            j++;
        }
        let pclTable = new Array;
        for (let i = 0; i <= 9; i++) {
            
            pclTable[i] = new Object;
            pclTable[i]['#'] = buffer[i]['0'] + ".";
            pclTable[i]['Name'] = buffer[i]['2'];
            pclTable[i]['Spiele'] = buffer[i]['3'] 
            + " - " + buffer[i]['4']
            + "-" + buffer[i]['5']
            + "-" + buffer[i]['6'];
            pclTable[i]['Tordifferenz'] = buffer[i]['7'] + ":" + buffer[i]['8'];
            pclTable[i]['Punkte'] = buffer[i]['10'];
        }1
        fs.writeFileSync("./data/pcl.json", JSON.stringify(pclTable));

        browser.close();

    })();
};

scrapePCL(pclAdress);
scrapeNGL(nglAdress);
scrapeESL(eslAdress);